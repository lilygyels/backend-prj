function addHabit() {
    var data = getFormData()

    fetch('/habit', {
        method: "POST",
        body: JSON.stringify(data),
        headers: {"Content-type": "application/json: charset=UTF-8"}
    }).then (response1 => {
        if (response1.ok) {
            fetch('/habit/'+slno)
            .then(response2 => response2.text())
            .then(data => showHabit(data))
        } else {
            throw new Error(response1.statusText)
        }
    }).catch(e => {
        alert(e)
    })
    resetform();
}


window.onload = function () {
    fetch("/habits")
    .then(response => response.text())
    .then(data => showHabits(data))
}

function showHabit(data) {
    const habit = JSON.parse(data)
    newRow(habit)
}

function showHabits(data) {
    const habits = JSON.parse(data)
    habits.forEach(stud => {
        newRow(stud)
    });
}


//set form fields to empty
function resetform() {
    document.getElementById("slno").value = "";
    document.getElementById("habitname").value = "";
    document.getElementById("description").value = "";
}


function newRow(habit) {
    //Find a <table> element with id="myTable":
    var table = document.getElementById("myTable");

    //create an empty <tr> element and add to the last position of the table:
    var row = table.insertRow(table.length)

    //insert new cells (<td>elements) at the 1st and 2nd position of the "new" <tr> element:
    var td = []
    for (i=0; i<table.rows[0].cells.length; i++) {
        td[i] = row.insertCell(i);
    }

    //Add student detail to the new cells:
    td[0].innerHTML = habit.slno;
    td[1].innerHTML = habit.habitname;
    td[2].innerHTML = habit.description;
    td[3].innerHTML = '<input type="button" onclick="deleteHabit(this)" value="delete" id="button-1">';
    td[4].innerHTML = '<input type="button" onclick="updateHabit(this)" value="edit" id="button-2">';
}

var selectedRow = null;

function updateHabit(r) {
    selectedRow = r.parentElement.parentElement;

    //fill in the form fields with the selected row data
    document.getElementById("slno").value = selectedRow.cells[0].innerHTML;
    document.getElementById("habitname").value = selectedRow.cells[1].innerHTML;
    document.getElementById("description").value = selectedRow.cells[2].innerHTML;

    var btn = document.getElementById("button-add")
    slno = selectedRow.cells[0].innerHTML;
    if (btn) {
        btn.innerHTML = "Update";
        btn.setAttribute("onclick", "update(slno)");
    }
}

function getFormData() {
    var formData = {
        slno : parseInt(document.getElementById("slno").value),
        habitname : document.getElementById("habitname").value,
        description : document.getElementById("description").value,
    }
    return formData
}

function update(slno) {
    //data to be sent to the UPDATE request
    var newData = getFormData()

    fetch('/habit/'+slno, {
        method: "PUT",
        body: JSON.stringify(newData),
        headers: {"Content-type": "application/json; charset=UTF-8"}
    }).then (res => {
        if (res.ok) {
            //fill in selected row with updated value
            selectedRow.cells[0].innerHTML = newData.slno;
            selectedRow.cells[1].innerHTML = newData.habitname;
            selectedRow.cells[2].innerHTML = newData.description;
            //set to previous value
            var button = document.getElementById("button-add");
            button.innerHTML = "Add";
            button.setAttribute("onclick", "addHabit()");
            selectedRow = null;

            resetform();
        } else {
            alert("Server: Update request error.")
        }
    })
}

function deleteHabit(r){
    if (confirm('Are you sure you want to DELETE this?')){
        selectedRow = r.parentElement.parentElement;
        slno = selectedRow.cells[0].innerHTML;

        fetch('/habit/'+slno, {
            method: "DELETE",
            headers: {"Content-type": "application/json; charset=UTF-8"}
        });
        var rowIndex = selectedRow.rowIndex;
        if (rowIndex>0){
            document.getElementById("myTable").deleteRow(rowIndex);
        }
        selectedRow = null;
    }
}
