package model

import postgres "myapp/datastore/postgres"

type Habit struct {
	SlNo        int64  `json:"slno"`
	HabitName   string `json:"habitname"`
	Description string `json:"description"`
}

const queryInsertHabit = "INSERT INTO habit(slno, habitname, description) VALUES ($1, $2, $3);"

func (s *Habit) Create() error {
	_, err := postgres.Db.Exec(queryInsertHabit, s.SlNo, s.HabitName, s.Description)
	return err
}

const queryGetHabit = "SELECT slno, habitname, description FROM habit WHERE slno= $1;"

func (s *Habit) Read() error {
	return postgres.Db.QueryRow(queryGetHabit, s.SlNo).Scan(&s.SlNo, &s.HabitName, &s.Description)
}

const queryUpdate = "UPDATE habit SET slno=$1, habitname=$2, description=$3 WHERE slno=$4 RETURNING slno;"

func (s *Habit) Update(oldID int64) error {
	err := postgres.Db.QueryRow(queryUpdate, s.SlNo, s.HabitName, s.Description, oldID).Scan(&s.SlNo)
	return err
}

const queryDeleteHabit = "DELETE FROM habit WHERE slno=$1;"

func (s *Habit) Delete() error {
	if _, err := postgres.Db.Exec(queryDeleteHabit, s.SlNo); err != nil {
		return err
	}
	return nil
}

func GetAllHabits() ([]Habit, error) {
	rows, getErr := postgres.Db.Query("SELECT * from habit;")
	if getErr != nil {
		return nil, getErr
	}

	//create a slice of type student
	habits := []Habit{}

	for rows.Next() {
		var s Habit
		dbErr := rows.Scan(&s.SlNo, &s.HabitName, &s.Description)
		if dbErr != nil {
			return nil, dbErr
		}

		habits = append(habits, s)
	}
	rows.Close()
	return habits, nil
}
