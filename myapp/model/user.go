package model

import postgres "myapp/datastore/postgres"

type User struct {
	FirstName string
	LastName  string
	Email     string
	Password  string
}

const queryInsertUser = "INSERT INTO user1(firstname, lastname, email, password) VALUES($1, $2, $3, $4);"

func (u *User) Create() error {
	_, err := postgres.Db.Exec(queryInsertUser, u.FirstName, u.LastName, u.Email, u.Password)
	return err
}

const queryGetUser = "SELECT email, password FROM user1 WHERE email=$1 and password=$2;"

func (u *User) Get() error {
	return postgres.Db.QueryRow(queryGetUser, u.Email, u.Password).Scan(&u.Email, &u.Password)
}
