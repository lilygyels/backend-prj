package routes

import (
	"log"
	"myapp/controller"
	"net/http"

	"github.com/gorilla/mux"
)

func InitializeRoutes() {
	router := mux.NewRouter()

	// habits routes
	router.HandleFunc("/habit", controller.AddHabit).Methods("POST")
	router.HandleFunc("/habit/{sid}", controller.GetHabit).Methods("GET")
	router.HandleFunc("/habit/{sid}", controller.UpdateHabit).Methods("PUT")
	router.HandleFunc("/habit/{sid}", controller.DeleteHabit).Methods("DELETE")

	router.HandleFunc("/habits", controller.GetAllHab)

	// user login and signup
	router.HandleFunc("/signup", controller.Signup).Methods("POST")
	router.HandleFunc("/login", controller.Login).Methods("POST")

	router.HandleFunc("/logout", controller.Logout)

	fhandler := http.FileServer(http.Dir("./index"))
	router.PathPrefix("/").Handler(fhandler)

	log.Println("Application running on port 8080...")
	log.Fatal(http.ListenAndServe(":8080", router))
}
