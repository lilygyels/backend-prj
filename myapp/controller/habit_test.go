package controller

import (
	"bytes"
	"io"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAddHabit(t *testing.T) {
	url := "http://localhost:8080/habit"

	var jsonStr = []byte(`{"slno":44, "habitname":"sleepp", "description":"10 pages everyday"}`)

	req, _ := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))

	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)

	if err != nil {
		panic(err)
	}

	defer resp.Body.Close()

	body, _ := io.ReadAll(resp.Body)

	assert.Equal(t, http.StatusCreated, resp.StatusCode)
	expResp := `{"status": "habit added"}`

	assert.JSONEq(t, expResp, string(body))
}

func TestGetHabit(t *testing.T) {
	c := &http.Client{}
	r, _ := c.Get("http://localhost:8080/habit/2")
	body, _ := io.ReadAll(r.Body)
	assert.Equal(t, http.StatusOK, r.StatusCode)
	expResp := `{"slno":2, "habitname":"Read", "description":"10 pages everyday"}`
	assert.JSONEq(t, expResp, string(body))
}

func TestDeleteHabit(t *testing.T) {
	url := "http://localhost:8080/habit/44"

	req, _ := http.NewRequest("DELETE", url, nil)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	body, _ := io.ReadAll(resp.Body)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	expResp := `{"status": "deleted"}`
	assert.JSONEq(t, expResp, string(body))

}

func TestHabitNotFound(t *testing.T) {
	assert := assert.New(t)
	c := &http.Client{}
	r, _ := c.Get("http://localhost:8080/habit/2222")
	body, _ := io.ReadAll(r.Body)
	assert.Equal(http.StatusNotFound, r.StatusCode)
	expResp := `{"error":"Habit not found"}`
	assert.JSONEq(expResp, string(body))
}
